#ifndef VARIABLES // avoid double inclusions
#define VARIABLES

#include "Arduino.h"

/* Note masks are updated from MIDI. The array indices correspond to the pi
 * that's low for each mask. This can be seen as a kind of boolean array split
 * over two bitmasks so that they can be directly set without additional
 * processing.*/
extern byte noteStatesB[9];
extern byte noteStatesD[9];

// MIDI variables
extern const uint8_t
    MIDILOW; // lowest MIDI note. This is the lowest note in the matrix, but the
             // lowest playable note is actually 47.

/* Mask templates for PORTD and PORTB. The 1 in each mask corresponds to the pin
 * that has to be pulled low. These all have a length of 49 (the amount of
 * keys). The key number can be used as an index to find the index to edit in
 * noteStates[] and what bit to flip. The index in maskindices specifies for
 * which cycle the pin has to be pulled low. */
extern const byte NOTEMASKSD[];
extern const byte NOTEMASKSB[];
extern const uint8_t NOTEMASKINDICES[];

#endif // !VARIABLES
