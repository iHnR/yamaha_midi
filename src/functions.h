#ifndef FUNCTIONS // avoid double inclusions
#define FUNCTIONS

#include "variables.h"
#include <Arduino.h>

/* Find the pin among the 'leader' pins (0-8) that is currently low Returns 9 if
 * none are low */
uint8_t findLowPin();

/* Set the bitmasks to pull down the currently relevant pins. This functions
 * only needs global parameters and thus needs no arguments. */
void setBitMasks();

// Set a note's state in the bitmask vector
void setNoteState(uint8_t midiValue, bool state);

/* This simply calls 'setNoteState' with value 'true' (as long as the message
 * contains a valid note) */
void handleNoteOn(byte channel, byte pitch, byte velocity);

/* This simply calls 'setNoteState' with value 'false' (as long as the message
 * contains a valid note)*/
void handleNoteOff(byte channel, byte pitch, byte velocity);

#endif // !DEBUG
