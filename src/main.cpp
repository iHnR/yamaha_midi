#include <Arduino.h>
#include <MIDI.h>
#include <functions.h>

/* Create MIDI object? I copied this line.
 * I really don't know what it's suposed to do exactly.
 * Probably just instantiate a global MIDI object. */
MIDI_CREATE_DEFAULT_INSTANCE();

void setup() {
  // set all bitmasks low. i.e: No note is active.
  for (int i = 0; i < 9; ++i) {
    noteStatesB[i] = 0b00000000;
    noteStatesD[i] = 0b00000000;
  }

  /* Set all pins to input. The output pins will be set to output at the moment
   * they need to perform a note. */
  DDRB = 0b00000000;
  DDRC = 0b00000000;
  DDRD = 0b00000000;

  /*Set all output values to low, just to be sure
    This is most likely unnecessary since this is
    the default. */
  PORTB = 0b00000000;
  PORTC = 0b00000000;
  PORTD = 0b00000000;

  // Serial for MIDI
  MIDI.begin(MIDI_CHANNEL_OMNI); // Start MIDI and listen to every channel

  /* For debugging it can be usefull to use a program called 'ttymidi'
   * to send midi messages directly over the Arduino's built in serial.
   * This requires overwriting the baud rate.*/
  // Serial.begin(115200);

  /* The MIDI library provides a useful way to bind functions to specific MIDI
   * events. */
  MIDI.setHandleNoteOn(
      handleNoteOn); // execute function when note on message is recieved
  MIDI.setHandleNoteOff(
      handleNoteOff); // execute function when note off message is recieved
  // All other messages are simply ignored.
}

void loop() {
  // Set the pins according to internal state and the keyboard's state
  setBitMasks();

  /* Update the internal state using MIDI.
   * This makes sure the handles defined in the setup get triggered. */
  MIDI.read();
}
