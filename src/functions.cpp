#include "functions.h"
#include "variables.h"
#include <Arduino.h>

uint8_t findLowPin() {
  /* Find the index of the the pin that is currently set low.
   * Assumes only one pin can be low at a time. Otherwise the first found will
   * be returned. Return 9 when no pin is low.
   *  + inPins [0-4] correspond to bits [4-0] on PINC
   *  + inPins [5-8] correspond to bits [5-2] on PINB */
  byte inPins;
  uint8_t p;                             // pin number
  if ((inPins = (~PINC) & 0b00011111)) { // if low pin is in C register
    inPins <<= 3;                        // Shift left to starting position
    p = 0;
    while (inPins <<= 1)
      p++;    // shift left until byte is zero
    return p; // p has the right value when the byte is zeroed out
  } else if ((inPins = ((~PINB) & 0b00111100))) { // if low pin is in B register
    inPins <<= 2;
    p = 5;
    while (inPins <<= 1)
      p++;
    return p;
  } else {
    return 9; // 9 specifies that none of the pins are low
  }
}

void setBitMasks() {
  /* Set DDR registers to their corresponding bitmasks
   * This sets the pinMode of the pins*/
  uint8_t lowPin = findLowPin();
  if (lowPin == 9) { // none of inPins are low -> all outPins are set to input
    DDRB = 0b00000000;
    DDRD = 0b00000000;
  } else { // some inPin is low -> look for it's current bitmasks
    DDRB = noteStatesB[lowPin];
    DDRD = noteStatesD[lowPin];
  }
}

void setNoteState(uint8_t midiValue, bool state) {
  /* This function takes a midi note and sets its corresponding state in the
   * bitmask matrix. To do this, we use the MIDIMASKs. The array MIDIMAKINDICES
   * tells us which of the leading cycles we should be setting. The arrays
   * NOTEMASKSD and NOTEMASKSB tell us the bit that has to be flipped for this
   * note For each note, there are two operations, one of which is always
   * unnecessary. This could be avoided but I think the check would be more
   * expensive than simply performing the action anyway.
   */
  uint8_t arIndex =
      midiValue -
      MIDILOW; // bitMask template array starts at the lowest midi note
  uint8_t maskIndex = NOTEMASKINDICES[arIndex]; // find where in the state array
                                                // to adjust the bitmask
  if (state) {                                  // Set bit to 1 to enable note
    noteStatesD[maskIndex] |= NOTEMASKSD[arIndex];
    noteStatesB[maskIndex] |= NOTEMASKSB[arIndex];
  } else { // Set bit to 0 to disable note
    noteStatesD[maskIndex] &= ~NOTEMASKSD[arIndex];
    noteStatesB[maskIndex] &= ~NOTEMASKSB[arIndex];
  }
}

void handleNoteOn(byte channel, byte pitch, byte velocity) {
  /* First we check if the note is in the playable range.
   * If that's the case, we set the noteState to true. */
  if (pitch > 47 && pitch < 97)
    setNoteState(pitch, true);
}

void handleNoteOff(byte channel, byte pitch, byte velocity) {
  /* First we check if the note is in the playable range.
   * If that's the case, we set the noteState to false. */
  if (pitch > 47 && pitch < 97)
    setNoteState(pitch, false);
}
