#include "variables.h"
#include "Arduino.h"

/* Note masks are updated from MIDI. The array indices correspond to the pin
 * that's low for each mask. This can be seen as a 9 by 6 boolean array (with
 * some bits we don't care about). All notes 6 could be on a single register,
 * which would simplify the code, but some pins on my header broke off and I
 * didn't have a spare, so that's why I'm using 2. */
byte noteStatesB[9];
byte noteStatesD[9];

// MIDI variables
const uint8_t MIDILOW =
    43; // lowest MIDI note. This is the lowest note in the matrix, but the
        // lowest playable note is actually 47.

/* NOTEMASKSA/B contain bitmasks for follower pins corresponding with their
 * midi note (index by midiNumber-MIDILOW). NOTEMASKINDICES contains the the
 * information on which leader pin the note corresponds to.*/
const byte NOTEMASKSD[] = {
    0b00010000, 0b00100000, 0b01000000, 0b10000000, 0b00000000, 0b00000000,
    0b00010000, 0b00100000, 0b01000000, 0b10000000, 0b00000000, 0b00000000,
    0b00010000, 0b00100000, 0b01000000, 0b10000000, 0b00000000, 0b00000000,
    0b00010000, 0b00100000, 0b01000000, 0b10000000, 0b00000000, 0b00000000,
    0b00010000, 0b00100000, 0b01000000, 0b10000000, 0b00000000, 0b00000000,
    0b00010000, 0b00100000, 0b01000000, 0b10000000, 0b00000000, 0b00000000,
    0b00010000, 0b00100000, 0b01000000, 0b10000000, 0b00000000, 0b00000000,
    0b00010000, 0b00100000, 0b01000000, 0b10000000, 0b00000000, 0b00000000,
    0b00010000, 0b00100000, 0b01000000, 0b10000000, 0b00000000, 0b00000000};
const byte NOTEMASKSB[] = {
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000010,
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000010,
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000010,
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000010,
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000010,
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000010,
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000010,
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000010,
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000010};
const uint8_t NOTEMASKINDICES[] = {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2,
                                   2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4,
                                   4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6,
                                   7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8};
