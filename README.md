## Overview
This is a quick description that I might extend if there is any interest. Feel free to reach out if you want to use it but can't figure it out. 

This is a little program I wrote to control my old Yamaha portasound PSS-570 over midi using an Arduino Uno. It works by inserting notes into the key matrix. It's all quite hardcoded for speed purposes, so it's not very adaptable for other keyboards. It is extensively commented so it should be understandable and might help if you want to do something similar.

This was built using PlatformIO (strongly recommended) and the [PlatformIO vim plugin](https://github.com/normen/vim-pio), but you should be able to just copy everything into an Arduino project and use it that way. (possibly you need to rename the files) 

## Hardware
It depends on what you have. 

The key matrix is 9 by 6. It's connected to the main board via a ribbon. It's fairly obvious once you get inside. I forgot which ones are which exactly but you can experiment by jumping the pads and figure it out. The 6 pins go to pin 4-9 on the Arduino. The 9 go to pins 10-13 and A0-A4. Again I forgot in which order.

For power, I just found a pin that gives 12V only when the keyboard turns on. Connected that to a DC-DC converter to get 5V and connected that to the 5V pin on the arduino. I do not recommend using the Vin pin on the Arduino since it just burns the excess power. The internal power supply of this keyboard is also not regulated and could provide voltages quite a bit higher than 12V. You can find some obvious ground pins inside.

For midi in, you could just connect a midi port to the rx pin, but it's sstandard to use an optocoupler for ground loop protection. I used [this schematic](https://2.bp.blogspot.com/-sQ8Uw7x-Vs0/VOatHJCRTGI/AAAAAAAAA9E/yD_TdRw_QKo/s1600/MIDI_Input_Schematic.png).
